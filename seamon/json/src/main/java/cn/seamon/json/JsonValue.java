package cn.seamon.json;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * 引用类型对象JsonValue
 * @author seamon
 * @version 1.0
 */
public final class JsonValue {

    /**
     * 无参构造函数
     */
    public JsonValue() {
    }

    /**
     * 有参构造函数
     */
    public JsonValue(Object value) throws Exception {
        this.setValue(value);
    }

    /**
     * 基础值类型集合
     */
    private static final Class[] baseDataClasses = {boolean.class, byte.class, char.class, double.class, float.class, int.class, long.class, short.class};

    /**
     * 支持的数据类型
     */
    private static final Class[] supportedClasses = {String.class, BigDecimal.class, BigInteger.class, JsonObject.class, JsonArray.class, Date.class, java.sql.Date.class, boolean.class, byte.class, char.class, double.class, float.class, int.class, long.class, short.class};

    /**
     * JsonValue对象的值
     */
    private Object value = null;

    /**
     * JsonValue数据类型
     */
    private Class valueClass = null;

    /**
     * JsonValue对象的值
     *
     * @return JsonValue对象的值
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * JsonValue对象的泛型值
     *
     * @return JsonValue对象的泛型值
     */
    public <T> T getValue(T type) {
        if (this.value == null) {
            return null;
        } else {
            return (T) this.value;
        }
    }

    /**
     * JsonValue数据类型
     *
     * @return JsonValue数据类型
     */
    public Class getValueClass() {
        return this.valueClass;
    }

    /**
     * 设置JsonValue对象的值
     */
    public void setValue(Object value) throws Exception {
        if (value == null) {
            this.value = null;
            this.valueClass = null;
            return;
        }
        Class c = this.value.getClass();
        if (!Arrays.asList(supportedClasses).contains(c)) {
            throw new Exception("不支持的类型！" + c.getName());
        }
        this.value = value;
        this.valueClass = value.getClass();
    }

    /**
     * 对象转为字符串
     *
     * @return JsonValue转换后的字符串
     */
    @Override
    public String toString() {
        if (this.valueClass == String.class || this.valueClass == char.class) {
            StringBuilder sb = new StringBuilder();
            sb.append("\"");
            sb.append(new String(this.value.toString()));
            sb.append("\"");
            return sb.toString();
        }
        if (this.valueClass == boolean.class) {
            if ((boolean) this.value)
                return "true";
            return "false";
        }
        if (this.valueClass == Date.class) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            StringBuilder sb = new StringBuilder();
            sb.append("\"");
            sb.append(dateFormat.format((Date) this.value));
            sb.append("\"");
            return sb.toString();
        }
        if (this.valueClass == java.sql.Date.class) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            StringBuilder sb = new StringBuilder();
            sb.append("\"");
            sb.append(dateFormat.format(((java.sql.Date) this.value).getTime()));
            sb.append("\"");
            return sb.toString();
        }
        return this.value.toString();
    }

    /**
     * 比较当前实例和指定对象实例的字符串表示是否相等
     *
     * @return 比较结果
     */
    public boolean isEqualsWith(Object object) {
        if (object == null) {
            return this.value == null;
        }
        return this.toString() == object.toString();
    }

    /**
     * 复制当前实例
     *
     * @return JsonValue新实例
     */
    public JsonValue copy() throws Exception {
        if (this.value == null) {
            return new JsonValue();
        }
        if (this.valueClass == String.class) {
            return new JsonValue(new String(this.value.toString()));
        }
        if (this.valueClass == BigDecimal.class) {
            return new JsonValue(new BigDecimal(((BigDecimal) this.value).toString()));
        }
        if (this.valueClass == BigInteger.class) {
            return new JsonValue(new BigInteger(((BigInteger) this.value).toString()));
        }
        if (Arrays.asList(baseDataClasses).contains(this.valueClass))
            return new JsonValue(this.value);
        if (this.valueClass == Date.class) {
            return new JsonValue(((Date) this.value).clone());
        }
        if (this.valueClass == java.sql.Date.class) {
            return new JsonValue(((java.sql.Date) this.value).clone());
        }
        if (this.valueClass == JsonObject.class)
            return new JsonValue(((JsonObject) this.value).copy());
        if (this.valueClass == JsonArray.class)
            return new JsonValue(((JsonArray) this.value).copy());
        return new JsonValue(this.value);
    }
}