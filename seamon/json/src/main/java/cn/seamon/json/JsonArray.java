package cn.seamon.json;

import java.util.*;

/**
 * 引用类型对象JsonArray
 * @author seamon
 * @version 1.0
 */
public final class JsonArray implements Iterable<Object>, Iterator<Object> {

    /**
     * 构造函数
     */
    public JsonArray() {
        this.jsonValues = new ArrayList<>();
    }

    /**
     * JsonValue列表
     */
    private ArrayList<JsonValue> jsonValues;

    /**
     * 迭代器的索引
     */
    protected int index = 0;

    /**
     * 获取集合元素个数
     *
     * @return 集合元素个数
     */
    public int count() {
        return this.jsonValues.size();
    }

    /**
     * 判断JsonArray中是否包含指定的Value
     *
     * @return 判断结果
     */
    public boolean contains(Object value) {
        boolean isContain = false;
        for (JsonValue jv : this.jsonValues) {
            if ((value.getClass() == JsonValue.class && jv.equals(value)) || jv.getValue().equals(value)) {
                isContain = true;
                break;
            }
        }
        return isContain;
    }

    /**
     * 根据索引获取集合中的Value
     *
     * @return 检索到的Value
     */
    public Object get(int index) {
        return this.jsonValues.get(index).getValue();
    }

    /**
     * 取Value集合
     *
     * @return Value集合
     */
    public Collection<Object> getValues() {
        Collection<Object> values = new ArrayList<>();
        for (JsonValue jv : this.jsonValues) {
            Object value = jv.getValue();
            if (!values.contains(value))
                values.add(value);
        }
        return values;
    }

    /**
     * 像集合添加一个Value
     *
     * @return 是否成功
     */
    public boolean add(Object value) throws Exception {
        if (value.getClass() == JsonValue.class) {
            return this.jsonValues.add((JsonValue) value);
        } else {
            return this.jsonValues.add(new JsonValue(value));
        }
    }

    /**
     * 像集合添加一个Value集合
     *
     * @return 是否成功
     */
    public boolean addAll(Collection<Object> values) throws Exception {
        ArrayList<JsonValue> jvs = new ArrayList<>();
        for (Object v : values) {
            if (v.getClass() == JsonValue.class)
                jvs.add((JsonValue) v);
            else
                jvs.add(new JsonValue(v));
        }
        return this.jsonValues.addAll(jvs);
    }

    /**
     * 根据索引从集合中移除一个Value并将其返回给调用者
     *
     * @return 移除的Value
     */
    public Object remove(int index) {
        return this.jsonValues.remove(index).getValue();
    }

    /**
     * 从集合中移除一个Value
     *
     * @return 是否成功
     */
    public boolean remove(Object value) {
        if (value.getClass() == JsonValue.class)
            return this.jsonValues.remove((JsonValue) value);
        else {
            JsonValue removeJsonValue = null;
            for (JsonValue jv : this.jsonValues) {
                if (jv.getValue().equals(value)) {
                    removeJsonValue = jv;
                    break;
                }
            }
            return this.jsonValues.remove(removeJsonValue);
        }
    }

    /**
     * 从集合中移除一个Value集合
     *
     * @return 是否成功
     */
    public boolean removeAll(Collection<Object> values) {
        HashMap<Object, ArrayList<JsonValue>> jvs = new HashMap<>();
        for (JsonValue jv : this.jsonValues) {
            Object obj = jv.getValue();
            if (!jvs.keySet().contains(obj))
                jvs.put(obj, new ArrayList<>());
            jvs.get(obj).add(jv);
        }
        ArrayList<JsonValue> removeJsonValues = new ArrayList<>();
        for (Object v : values) {
            if (v.getClass() == JsonValue.class) {
                if (this.jsonValues.contains((JsonValue) v)) {
                    removeJsonValues.add((JsonValue) v);
                }
            } else {
                if (jvs.keySet().contains(v))
                    removeJsonValues.addAll(jvs.get(v));
            }
        }
        return this.jsonValues.removeAll(removeJsonValues);
    }

    /**
     * 比较当前实例和指定对象实例的字符串表示是否相等
     *
     * @return 比较结果
     */
    public boolean isEqualsWith(Object object) {
        return this.toString() == object.toString();
    }

    /**
     * 复制当前JsonArray实例
     *
     * @return 生成的JsonArray新实例
     */
    public JsonArray copy() throws Exception {
        JsonArray ja = new JsonArray();
        for (JsonValue jv : this.jsonValues)
            ja.add(jv.copy());
        return ja;
    }

    /**
     * 将JsonArray实例转为其字符串表示
     *
     * @return JsonArray实例的字符串表示
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        int length = this.jsonValues.size();
        for (int i = 0; i < length; i++) {
            if (i > 0)
                sb.append(",");
            sb.append(this.jsonValues.get(i).toString());
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public Iterator<Object> iterator() {
        this.index = 0;
        return this;
    }

    @Override
    public boolean hasNext() {
        return this.index < this.jsonValues.size();
    }

    @Override
    public JsonValue next() {
        if (this.hasNext()) {
            return this.jsonValues.get(this.index++);
        }

        throw new NoSuchElementException("only " + this.jsonValues.size() + " elements");
    }
}