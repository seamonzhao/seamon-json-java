package cn.seamon.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 引用类型对象JsonConvert，静态方法用于各类型之间的转换
 * @author seamon
 * @version 1.0
 */
public final class JsonConvert {


    private static JsonConvert current = null;
    private static JsonConvert getCurrent() {
        if (current == null)
            current = new JsonConvert();
        return current;
    }

    private Index getNewIndex(int val) {
        return new JsonConvert.Index(val);
    }

    /**
     * java基本数据类型
     */
    private static final Class[] baseDataClasses = {boolean.class, byte.class, char.class, BigDecimal.class, double.class, float.class, int.class, long.class, short.class, String.class};

    /**
     * java基本数据类型
     */
    private static final List BaseDataClasses = Arrays.asList(baseDataClasses);

    /**
     * 左大括号
     */
    private static final char leftBrace = '{';

    /**
     * 右大括号
     */
    private static final char rightBrace = '}';

    /**
     * 左中括号
     */
    private static final char leftBracket = '[';

    /**
     * 右中括号
     */
    private static final char rightBracket = ']';

    /**
     * 单引号
     */
    private static final char singleQuotes = '\'';

    /**
     * 双引号
     */
    private static final char doubleQuotes = '"';

    /**
     * 反斜杠
     */
    private static final char backSlash = '\\';

    /**
     * 冒号
     */
    private static final char colon = ':';

    /**
     * 逗号
     */
    private static final char comma = ',';

    /**
     * 加号
     */
    private static final char plus = '+';

    /**
     * 减号
     */
    private static final char minus = '-';

    /**
     * 小数点
     */
    private static final char dPoint = '.';

    /**
     * JsonObject、JsonArray开始字符
     */
    private static final char[] objBeginChar = {leftBrace, leftBracket};

    /**
     * JsonObject、JsonArray开始字符
     */
    private static final List ObjBeginChar = Arrays.asList(objBeginChar);

    /**
     * JsonObject、JsonArray结束字符
     */
    private static final char[] objEndChar = {rightBrace, rightBracket};

    /**
     * JsonObject、JsonArray结束字符
     */
    private static final List ObjEndChar = Arrays.asList(objEndChar);

    /**
     * 结束符
     */
    private static final char[] valueEndChar = {'`', '~', '!', '@', '#', /*'$',*/ '%', '^', '&', '*', '(', ')'/*, '+', '-', '_'*/, '=', ';', ':'/*, '\'', '"'*/, ',', '<', '.', '>', '/', '?', '\\', '|'};

    /**
     * 结束符
     */
    private static final List ValueEndChar = Arrays.asList(valueEndChar);

    /**
     * 无意义的间隔符
     */
    private static final char[] noSenceChar = {' '/*, '\a'*/, '\f', '\n', '\r', '\t'/*, '\v'*/};

    /**
     * 无意义的间隔符
     */
    private static final List NoSenceChar = Arrays.asList(noSenceChar);

    /**
     * 根据指定String转为JsonObject
     *
     * @return 转换后的JsonObject
     */
    public static JsonObject stringToJsonObject(String jsonString) throws Exception {
        return JsonConvert.createJsonObjectByString(JsonConvert.getCurrent().getNewIndex(0), jsonString.toCharArray());
    }

    /**
     * 根据指定String转为JsonArray
     *
     * @return 转换后的JsonArray
     */
    public static JsonArray stringToJsonArray(String jsonString) throws Exception{
        return JsonConvert.createJsonArrayByString(JsonConvert.current.getNewIndex(0), jsonString.toCharArray());
    }

    /**
     * 根据指定String转为Json（JsonObject或JsonArray）
     *
     * @return 转换后的Json对象（JsonObject或JsonArray）
     */
    public static Iterable stringToJson(String jsonString) throws Exception {
        Index index = JsonConvert.current.getNewIndex(0);
        char[] jsonCharArray = jsonString.toCharArray();
        if (index.now() >= jsonCharArray.length)
            return null;

        for (; index.now() < jsonCharArray.length; index.further()) {
            if (NoSenceChar.contains(jsonCharArray[index.now()]))
                continue;
            else
                break;
        }

        int iNow = index.now();
        if (index.now() >= jsonCharArray.length || JsonConvert.ObjEndChar.contains(jsonCharArray[index.now()]))
            return null;

        if (jsonCharArray[iNow] == leftBrace)
            return JsonConvert.createJsonObjectByString(index, jsonCharArray);
        else if (jsonCharArray[iNow] == leftBracket)
            return JsonConvert.createJsonArrayByString(index, jsonCharArray);
        else
            throw new Exception("must begin with \"{\" or \"[\"");
    }

    /**
     * 用于循环字符数组的索引类
     */
    private class Index {

        /**
         * 构造函数
         */
        private Index(int index) {
            this.i = index;
        }

        private int i;

        /**
         * 进一步
         *
         * @return 进一步后的索引值
         */
        private int further() {
            return ++this.i;
        }

        /**
         * 退一步
         *
         * @return 退一步后的索引值
         */
        private int back() {
            return --this.i;
        }

        /**
         * 取当前索引
         *
         * @return 当前索引值
         */
        private int now() {
            return this.i;
        }
    }

    /**
     * 从String对象中创建JsonObject
     *
     * @return 创建的JsonObject
     */
    private static JsonObject createJsonObjectByString(Index index, char[] jsonString) throws Exception {

        JsonObject jo = new JsonObject();

        if (index.now() >= jsonString.length)
            return jo;

        for (; index.now() < jsonString.length; index.further()) {
            if (NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;
        }

        int iNow = index.now();
        if (iNow >= jsonString.length || ObjEndChar.contains(jsonString[iNow]) || (ObjEndChar.contains(jsonString[jsonString.length - 1]) && jsonString.length - iNow <= 4) || (!ObjEndChar.contains(jsonString[jsonString.length - 1]) && jsonString.length - iNow < 4))
            return jo;

        if (jsonString[iNow] != leftBrace)
            throw new Exception("must begin with \"{\"!");

        index.further();

        for (; index.now() < jsonString.length; index.further()) {
            if (NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;
        }

        while (index.now() < jsonString.length && !ObjEndChar.contains(jsonString[index.now()])) {
            if (NoSenceChar.contains(jsonString[index.now()])) {
                index.further();
                continue;
            }
            JsonMember d = createJsonMemberByString(index, jsonString);
            if (d != null)
                jo.add(d);
        }
        if (index.now() < jsonString.length && jsonString[index.now()] == rightBrace)
            index.further();

        return jo;
    }

    /**
     * 从String对象中创建JsonMember
     *
     * @return 创建的JsonMember
     */
    private static JsonMember createJsonMemberByString(Index index, char[] jsonString) throws Exception {

        //去掉最左边的逗号、空格、回车、换行、制表符等字符
        for (; index.now() < jsonString.length; index.further()) {
            if (jsonString[index.now()] == comma || NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;
        }

        //开始读取字符
        if (index.now() < jsonString.length && !ObjEndChar.contains(jsonString[index.now()])) {
            JsonMember jm = new JsonMember();

            if (ObjBeginChar.contains(jsonString[index.now()])) {
                JsonValue v = JsonConvert.createJsonValueByString(false, index, jsonString);
                if (v == null)
                    return null;
                else {
                    jm.setKey("");
                    jm.setValue(v);
                    return jm;
                }
            }

            ArrayList key = new ArrayList();
            Boolean isQuotes = false; //当前读取的字符是否是在引号内
            Boolean isKeyDone = false;// Key是否读取完
            char quotes = ' '; //引号类型：单引号、双引号
            for (; index.now() < jsonString.length; index.further()) {
                if (key.size() == 0) {
                    if (isQuotes) {
                        if (jsonString[index.now()] == backSlash && (index.now() + 1) < jsonString.length && jsonString[index.now() + 1] == quotes) {
                            key.add(quotes);
                            index.further();
                            continue;
                        } else if (jsonString[index.now()] == quotes) {
                            key.add('\0');
                            isKeyDone = true;
                            isQuotes = false;
                            continue;
                        } else {
                            key.add(jsonString[index.now()]);
                            continue;
                        }
                    } else {
                        if (jsonString[index.now()] == singleQuotes || jsonString[index.now()] == doubleQuotes) {
                            isQuotes = true;
                            quotes = jsonString[index.now()];
                            continue;
                        } else {
                            key.add(jsonString[index.now()]);
                            continue;
                        }
                    }
                } else {
                    if (isKeyDone) {
                        if (isQuotes) {
                            if (jsonString[index.now()] == backSlash && (index.now() + 1) < jsonString.length && jsonString[index.now() + 1] == quotes) {
                                index.further();
                                continue;
                            } else if (jsonString[index.now()] == quotes) {
                                isQuotes = false;
                                quotes = ' ';
                                continue;
                            } else
                                continue;
                        } else {
                            if (jsonString[index.now()] == singleQuotes || jsonString[index.now()] == doubleQuotes) {
                                isQuotes = true;
                                quotes = jsonString[index.now()];
                                continue;
                            } else if (ObjEndChar.contains(jsonString[index.now()]))
                                break;
                            else if (jsonString[index.now()] != colon && !ObjBeginChar.contains(jsonString[index.now()]))
                                continue;
                            else //如果Json字符串符合Json格式，则最终会收敛到此处
                            {
                                JsonValue jv = JsonConvert.createJsonValueByString(false, index, jsonString);
                                if (jv == null)
                                    return null;
                                else {
                                    if (key.size() == 1 && (char) key.get(0) == '\0')
                                        jm.setKey("");
                                    else
                                        jm.setKey(String.join("", key));
                                    jm.setValue(jv);
                                    return jm;
                                }
                            }
                        }
                    } else {
                        if (isQuotes) {
                            if (jsonString[index.now()] == backSlash && (index.now() + 1) < jsonString.length && jsonString[index.now() + 1] == quotes) {
                                index.further();
                                key.add(jsonString[index.now()]);
                                continue;
                            } else if (jsonString[index.now()] == quotes) {
                                isKeyDone = true;
                                isQuotes = false;
                                quotes = ' ';
                                continue;
                            } else {
                                key.add(jsonString[index.now()]);
                                continue;
                            }
                        } else {
                            if (jsonString[index.now()] == singleQuotes || jsonString[index.now()] == doubleQuotes) {
                                isKeyDone = true;
                                isQuotes = true;
                                quotes = jsonString[index.now()];
                                continue;
                            } else if (JsonConvert.ObjEndChar.contains(jsonString[index.now()]))
                                break;
                            else if (JsonConvert.ObjBeginChar.contains(jsonString[index.now()])) {
                                isKeyDone = true;
                                index.back();
                                continue;
                            } else if (JsonConvert.NoSenceChar.contains(jsonString[index.now()]) || jsonString[index.now()] == plus || jsonString[index.now()] == minus) {
                                isKeyDone = true;
                                continue;
                            } else {
                                if (JsonConvert.ValueEndChar.contains(key.get(0))) {
                                    if (jsonString[index.now()] == (char) key.get(key.size() - 1)) {
                                        if (key.size() < 2)
                                            key.add(jsonString[index.now()]);
                                        continue;
                                    } else {
                                        isKeyDone = true;
                                        if (jsonString[index.now()] == colon)
                                            index.back();
                                        continue;
                                    }
                                } else {
                                    if (!JsonConvert.ValueEndChar.contains(jsonString[index.now()])) {
                                        key.add(jsonString[index.now()]);
                                        continue;
                                    } else {
                                        isKeyDone = true;
                                        if (jsonString[index.now()] == colon)
                                            index.back();
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null; //如果未收敛，则返回null
        } else
            return null;
    }

    /**
     * 从String对象中创建JsonArray
     *
     * @return 创建的JsonArray
     */
    private static JsonArray createJsonArrayByString(Index index, char[] jsonString) throws Exception {
        JsonArray ja = new JsonArray();

        if (index.now() >= jsonString.length)
            return ja;

        for (; index.now() < jsonString.length; index.further())
            if (JsonConvert.NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;

        if (index.now() >= jsonString.length || JsonConvert.ObjEndChar.contains(jsonString[index.now()]))
            return null;
        else if (jsonString[index.now()] != leftBracket)
            throw new Exception("must begin with \"[\"!");

        index.further();

        for (; index.now() < jsonString.length; index.further())
            if (JsonConvert.NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;

        while (index.now() < jsonString.length && !JsonConvert.ObjEndChar.contains(jsonString[index.now()])) {
            if (JsonConvert.NoSenceChar.contains(jsonString[index.now()])) {
                index.further();
                continue;
            }
            JsonValue v = JsonConvert.createJsonValueByString(true, index, jsonString);
            if (v != null)
                ja.add(v);
        }
        if (index.now() < jsonString.length && jsonString[index.now()] == rightBracket)
            index.further();

        return ja;
    }

    /**
     * 从String对象中创建JsonValue
     */
    private static JsonValue createJsonValueByString(boolean isInArray, Index index, char[] jsonString) throws Exception {
        //去掉最左边的逗号(JsonArray内的JsonValue)、冒号(JsonMember内的JsonValue)、空格、回车、换行、制表符等字符
        for (; index.now() < jsonString.length; index.further()) {
            if ((jsonString[index.now()] == comma && isInArray) || (jsonString[index.now()] == colon && !isInArray) || JsonConvert.NoSenceChar.contains(jsonString[index.now()]))
                continue;
            else
                break;
        }

        //开始读取字符
        if (index.now() < jsonString.length && !JsonConvert.ObjEndChar.contains(jsonString[index.now()])) {
            if (jsonString[index.now()] == leftBrace) {
                JsonObject d = JsonConvert.createJsonObjectByString(index, jsonString);
                if (d == null)
                    return null;
                else
                    return new JsonValue(d);
            } else if (jsonString[index.now()] == leftBracket) {
                JsonArray a = JsonConvert.createJsonArrayByString(index, jsonString);
                if (a == null)
                    return null;
                else
                    return new JsonValue(a);
            }

            boolean isQuotes = false; //当前读取的字符是否在引号内
            char quote = ' '; //引号类型：单引号、双引号
            ArrayList value = new ArrayList(); //读取到的value字符集
            for (; index.now() < jsonString.length; index.further()) {
                if (isQuotes) {
                    if (jsonString[index.now()] == backSlash && (index.now() + 1) < jsonString.length && jsonString[index.now() + 1] == quote) {
                        index.further();
                        value.add(quote);
                        continue;
                    } else if (jsonString[index.now()] == quote) {
                        index.further();
                        if (value.size() == 0)
                            return new JsonValue("");
                        else
                            return new JsonValue(String.join("", value));
                    } else {
                        value.add(jsonString[index.now()]);
                        continue;
                    }
                }

                if (value.size() == 0) {
                    if (jsonString[index.now()] == singleQuotes || jsonString[index.now()] == doubleQuotes) {
                        isQuotes = true;
                        quote = jsonString[index.now()];
                        continue;
                    } else {
                        value.add(jsonString[index.now()]);
                        continue;
                    }
                } else {
                    if (JsonConvert.ObjBeginChar.contains(jsonString[index.now()]) || JsonConvert.ObjEndChar.contains(jsonString[index.now()]))
                        return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    else if (jsonString[index.now()] == comma || JsonConvert.NoSenceChar.contains(jsonString[index.now()])) {
                        index.further();
                        return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    } else if (jsonString[index.now()] == singleQuotes || jsonString[index.now()] == doubleQuotes) {
                        return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    } else if (jsonString[index.now()] == dPoint) {
                        if (Pattern.matches("^[+-]?\\d*\\.?\\d+[eE][+-]?\\d*$", String.join("", value)) ||
                                Pattern.matches("^[+-]?\\d+\\.?\\d*[eE][+-]?\\d*$", String.join("", value))) {
                            //value.Add(jsonString[i]);
                            //continue;
                            return JsonConvert.notQuotesCharArrayToJsonValue(value);
                        } else if (!value.contains(dPoint)) {
                            value.add(jsonString[index.now()]);
                            continue;
                        } else
                            return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    } else if (JsonConvert.ValueEndChar.contains(jsonString[index.now()]) || (jsonString[index.now()] == plus || jsonString[index.now()] == minus)) {
                        if (Pattern.matches("^\\d*\\.?\\d+[+-]$", String.join("", value)) ||
                                Pattern.matches("^\\d+\\.?\\d*[+-]$", String.join("", value)))
                            return JsonConvert.notQuotesCharArrayToJsonValue(value);
                        else if (jsonString[index.now()] == (char) value.get(value.size() - 1)) {
                            value.add(jsonString[index.now()]);
                            continue;
                        } else if (jsonString[index.now()] == plus || jsonString[index.now()] == minus) {
                            if (Pattern.matches("^\\d*\\.?\\d+$", String.join("", value)) ||
                                    Pattern.matches("^\\d+\\.?\\d*$", String.join("", value))) //0.3-
                            {
                                value.add(jsonString[index.now()]);
                                continue;
                            } else if (Pattern.matches("^[+-]?\\d*\\.?\\d+[eE]$", String.join("", value)) ||
                                    Pattern.matches("^[+-]?\\d+\\.?\\d*[eE]$", String.join("", value)))//1.2e-10
                            {
                                value.add(jsonString[index.now()]);
                                continue;
                            } else
                                return JsonConvert.notQuotesCharArrayToJsonValue(value);
                        } else
                            return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    } else if (JsonConvert.ValueEndChar.contains((char) value.get(0)) != JsonConvert.ValueEndChar.contains(jsonString[index.now()])) {
                        if (Pattern.matches("^[+-]?\\.\\d*$", String.join("", value)) && Pattern.matches("^\\d$", String.valueOf(jsonString[index.now()]))) {
                            value.add(jsonString[index.now()]);
                            continue;
                        } else
                            return JsonConvert.notQuotesCharArrayToJsonValue(value);
                    } else {
                        value.add(jsonString[index.now()]);
                        continue;
                    }
                }
            }

            if (index.now() >= jsonString.length && value.size() > 0)
                return JsonConvert.notQuotesCharArrayToJsonValue(value);
            else
                return null;
        } else
            return null;
    }

    /**
     * 非引号内读取到的charList转成JsonValue
     *
     * @return 转换后的JsonValue
     */
    private static JsonValue notQuotesCharArrayToJsonValue(ArrayList value) throws Exception {
        String rv = String.join("", value);
        if (rv.toLowerCase() == "null" || rv.toLowerCase() == "dbnull")
            return new JsonValue(null);
        else if (rv.toLowerCase() == "true")
            return new JsonValue(true);
        else if (rv.toLowerCase() == "false")
            return new JsonValue(false);
        else if (Pattern.matches("^[+-]?\\d\\d*\\.?$", rv)) {
            long v = 0;
            try {
                v = Long.valueOf(rv.replace(".", ""));
            } catch (Exception e) {
                return new JsonValue(rv);
            }

            if (v <= Integer.MAX_VALUE && v >= Integer.MIN_VALUE)
                return new JsonValue((int) v);
            else
                return new JsonValue(v);
        } else if (Pattern.matches("^[+-]?\\d*\\.\\d+$", rv)) {
            double v = 0;
            try {
                v = Double.valueOf(rv);
            } catch (Exception e) {
                return new JsonValue(rv);
            }
            return new JsonValue(v);
        } else if (Pattern.matches("^\\d*\\.?\\d+[+-]$", rv) || Pattern.matches("^\\d+\\.?\\d*[+-]$", rv)) {
            BigDecimal v = new BigDecimal(0);
            try {
                v = BigDecimal.valueOf(Double.valueOf(rv));
            } catch (Exception e) {
                return new JsonValue(rv);
            }
            return new JsonValue(v);
        } else if (Pattern.matches("^[+-]?\\d*\\.?\\d+[eE][+-]?\\d+$", rv) || Pattern.matches("^[+-]?\\d+\\.?\\d*[eE][+-]?\\d+$", rv)) {
            double v = 0;
            try {
                String[] ss = rv.split("[eE]");
                double a = Double.valueOf(ss[0]);
                int b = Integer.valueOf(ss[1]);
                if (a == 0)
                    v = 0;
                else if (b == 0)
                    v = a;
                else
                    v = a * Math.pow(10, b);
            } catch (Exception e) {
                return new JsonValue(rv);
            }
            return new JsonValue(v);
        } else
            return new JsonValue(rv);
    }
}