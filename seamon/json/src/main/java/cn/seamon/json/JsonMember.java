package cn.seamon.json;

/**
 * 引用类型对象JsonMember
 * @author seamon
 * @version 1.0
 */
public final class JsonMember {

    /**
     * 无参构造函数
     */
    public JsonMember() {
        this.key = "";
    }

    /**
     * 带key参数的构造函数
     */
    public JsonMember(String key) {
        this.key = key;
    }

    /**
     * 带key和value参数的构造函数
     */
    public JsonMember(String key, Object value) throws Exception {
        this.key = key;
        if (value.getClass() == JsonValue.class)
            this.value = (JsonValue) value;
        else
            this.value = new JsonValue(value);
    }

    /**
     * JsonMember实例的Key
     */
    private String key;

    /**
     * 获取JsonMember实例的Key
     *
     * @return JsonMember实例的Key
     */
    private JsonValue value;

    /**
     * 获取JsonMember实例的key
     *
     * @return JsonMember实例的key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * 设置Key值
     */
    public void setKey(String key){
        this.key = key;
    }

    /**
     * 获取JsonMember实例的值
     *
     * @return JsonMember实例的值
     */
    public Object getValue() {
        return this.value.getValue();
    }

    /**
     * 获取JsonMember实例的泛型值
     *
     * @return JsonMember实例的泛型值
     */
    public <T> T getValue(T type) {
        return this.value.getValue(type);
    }

    /**
     * 获取JsonMember实例的值的JsonValue
     *
     * @return JsonMember实例的值的JsonValue
     */
    public JsonValue getJsonValue() {
        return this.value;
    }

    /**
     * 给Value赋值
     */
    public void setValue(Object object) throws Exception {
        if (object.getClass() == JsonValue.class) {
            this.value = (JsonValue) object;
        }
        this.value = new JsonValue(object);
    }

    /**
     * 对象转为字符串
     *
     * @return JsonMember转换后的字符串
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"");
        sb.append(this.key);
        sb.append("\":");
        sb.append(this.value.toString());
        return sb.toString();
    }

    /**
     * 比较当前实例和指定对象实例的字符串表示是否相等
     *
     * @return 比较结果
     */
    public boolean isEqualsWith(Object object) {
        return this.toString() == object.toString();
    }

    /**
     * 复制当前实例
     *
     * @return JsomMember新实例
     */
    public JsonMember copy() throws Exception {
        if (this == null)
            return null;
        if (this.value == null)
            return new JsonMember(new String(this.key));
        return new JsonMember(new String(this.key), this.value.copy());
    }
}