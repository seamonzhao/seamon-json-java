package cn.seamon.json;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 引用类型对象JsonObject
 * @author seamon
 * @version 1.0
 */
public final class JsonObject implements Iterable<JsonMember>, Iterator<JsonMember> {

    /**
     * 构造函数
     */
    public JsonObject() {
        this.jsonMembers = new ArrayList<>();
    }

    /**
     * JsonMember列表
     */
    private ArrayList<JsonMember> jsonMembers;

    /**
     * 迭代器的索引
     */
    protected int index = 0;

    /**
     * 获取JsonMember的Key集合
     *
     * @return JsonMember的Key集合
     */
    private ArrayList<String> getKeys() {
        ArrayList<String> keys = new ArrayList<>();
        for (JsonMember jm : this.jsonMembers)
            keys.add(jm.getKey());
        return keys;
    }

    /**
     * 获取集合元素个数
     *
     * @return 集合元素个数
     */
    public int count() {
        return this.jsonMembers.size();
    }

    /**
     * 判断JsonObject中是否包含指定的Key
     *
     * @return 判断结果
     */
    public boolean contains(String key) {
        boolean isContain = false;
        for (JsonMember jm : this.jsonMembers) {
            if (jm.getKey() == key) {
                isContain = true;
                break;
            }
        }
        return isContain;
    }

    /**
     * 判断JsonObject中是否包含指定的JsonMember
     *
     * @return 判断结果
     */
    public boolean contains(JsonMember jsonMember) {
        return this.jsonMembers.contains(jsonMember);
    }

    /**
     * 根据索引获取集合中的JsonMember
     *
     * @return 检索到的JsonMember
     */
    public JsonMember get(int index) {
        return this.jsonMembers.get(index);
    }

    /**
     * 根据Key获取集合中的JsonMember
     *
     * @return 检索到的JsonMember
     */
    public JsonMember get(String key) {
        JsonMember result = null;
        for (JsonMember jm : this.jsonMembers) {
            if (jm.getKey() == key) {
                result = jm;
                break;
            }
        }
        return result;
    }

    /**
     * 向集合添加一个JsonMember
     *
     * @return 是否成功
     */
    public boolean add(JsonMember jsonMember) throws Exception {
        if (this.jsonMembers.contains(jsonMember)) {
            throw new Exception("不可添加已在集合中的项！");
        }

        if (this.getKeys().contains(jsonMember.getKey())) {
            throw new Exception("已包含了具有相同Key的项！");
        }
        return this.jsonMembers.add(jsonMember);
    }

    /**
     * 向集合添加一个JsonMember集合
     *
     * @return 是否成功
     */
    public boolean addAll(Collection<JsonMember> jsonMembers) {
        return this.jsonMembers.addAll(jsonMembers);
    }

    /**
     * 向集合添加一个只包含Key的JsonMember
     *
     * @return 是否成功
     */
    public boolean add(String key) {
        return this.jsonMembers.add(new JsonMember(key));
    }

    /**
     * 向集合添加一个只包含Key的JsonMember集合
     *
     * @return 是否成功
     */
    public boolean addAll(String[] keys) {
        ArrayList<String> oriKeys = this.getKeys();
        for (String key : keys) {
            if (!oriKeys.contains(key)) {
                this.jsonMembers.add(new JsonMember(key));
            }
        }
        return true;
    }

    /**
     * 使用Key与Value向集合添加一个JsonMember
     *
     * @return 是否成功
     */
    public boolean add(String key, Object value) throws Exception {
        return this.jsonMembers.add(new JsonMember(key, value));
    }

    /**
     * 根据索引从集合中移除一个JsonMember并将其返回给调用者
     *
     * @return 移除的JsonMember实例
     */
    public JsonMember remove(int index) {
        return this.jsonMembers.remove(index);
    }

    /**
     * 从集合中移除一个JsonMember
     *
     * @return 是否成功
     */
    public boolean remove(JsonMember jsonMember) {
        return this.jsonMembers.remove(jsonMember);
    }

    /**
     * 从集合中移除一个JsonMember集合
     *
     * @return 是否成功
     */
    public boolean removeAll(Collection<JsonMember> jsonMembers) {
        return this.jsonMembers.removeAll(jsonMembers);
    }

    /**
     * 比较当前实例和指定对象实例的字符串表示是否相等
     *
     * @return 比较结果
     */
    public boolean isEqualsWith(Object object) {
        return this.toString() == object.toString();
    }

    /**
     * 将JsonObject实例转为其字符串表示
     *
     * @return JsonObject实例的字符串表示
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        int size = this.jsonMembers.size();
        for (int i = 0; i < size; i++) {
            JsonMember jm = this.jsonMembers.get(i);
            if (i > 0) {
                sb.append(",");
            }
            sb.append("\"");
            sb.append(jm.getKey());
            sb.append("\":");
            sb.append(jm.getJsonValue().toString());
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * 复制当前JsonObject实例
     *
     * @return 生成的JsonObject新实例
     */
    public JsonObject copy() throws Exception {
        JsonObject jo = new JsonObject();
        for (JsonMember jm : this.jsonMembers) {
            jo.add(jm.copy());
        }
        return jo;
    }

    @Override
    public Iterator<JsonMember> iterator() {
        this.index = 0;
        return this;
    }

    @Override
    public boolean hasNext() {
        return this.index < this.jsonMembers.size();
    }

    @Override
    public JsonMember next() {
        if (hasNext()) {
            return this.jsonMembers.get(this.index++);
        }

        throw new NoSuchElementException("only " + this.jsonMembers.size() + " elements");
    }
}